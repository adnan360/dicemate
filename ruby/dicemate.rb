#!/usr/bin/env ruby

# SecureRandom is safe to use in Ruby 2.5.0 and newer. This is written
# for those versions in mind, so don't let it run on versions before.
# Ref: https://paragonie.com/blog/2016/05/how-generate-secure-random-numbers-in-various-programming-languages#ruby-csprng
ruby_version_parts=RUBY_VERSION.split('.')
if ruby_version_parts[0].to_i < 2 or (ruby_version_parts[0].to_i == 2 and ruby_version_parts[1].to_i < 5)
	puts "Error: SecureRandom has issues on older Ruby versions. It may deliver weak passphrases, so not continuing. Please install Ruby 2.5.0 or newer and try again."
	exit(1)
end

require 'securerandom'

# Default config
# Word count
$config_words = 7
# Separator between words
$config_separator = ' '
# Prefix that sits before passphrase
$config_prefix = ''
# Suffix that sits after passphrase
$config_suffix = ''
# Length of index number on wordlist file
$config_number_length = 5
# Wordlist file
$config_wordlist_file = '../res/eff_large_wordlist.txt'

# Process CLI parameters and update config values as necessary
for i in 0 ... ARGV.length
	if ARGV[i] == '-h' or ARGV[i] == '--help'
		puts "usage: dicemate.rb [-h] [-w WORD_COUNT] [-s SEPARATOR] [-a PREFIX] [-z SUFFIX] [-f WORDLIST_FILE]
                   [-l LENGTH]

Generates a Diceware passphrase based on parameters.

optional arguments:
  -h, --help            show this help message and exit
  -w WORD_COUNT, --word-count WORD_COUNT
                        word count
  -s SEPARATOR, --separator SEPARATOR
                        separator between words
  -a PREFIX, --prefix PREFIX
                        prefix before passphrase
  -z SUFFIX, --suffix SUFFIX
                        suffix before passphrase
  -f WORDLIST_FILE, --wordlist-file WORDLIST_FILE
                        Diceware wordlist filename
  -l LENGTH, --index-length LENGTH
                        Diceware wordlist index length"
		exit
	elsif ARGV[i] == '-w' or ARGV[i] == '--word-count'
		$config_words = ARGV[i+1].to_i
	elsif ARGV[i] == '-s' or ARGV[i] == '--separator'
		$config_separator = ARGV[i+1]
	elsif ARGV[i] == '-a' or ARGV[i] == '--prefix'
		$config_prefix = ARGV[i+1]
	elsif ARGV[i] == '-z' or ARGV[i] == '--suffix'
		$config_suffix = ARGV[i+1]
	elsif ARGV[i] == '-f' or ARGV[i] == '--wordlist-file'
		$config_wordlist_file = ARGV[i+1]
	elsif ARGV[i] == '-l' or ARGV[i] == '--index-length'
		$config_number_length = ARGV[i+1].to_i
	end
end

# Get a dice roll number (1 through 6).
def get_dice_roll()
	return ( SecureRandom.random_number(6) + 1 )
end

# Get a Diceware index number which can be looked up on wordlist file.
def get_diceware_number()
	ret = ''
	for i in 1..$config_number_length
		ret += get_dice_roll().to_s
	end
	return ret
end

# Open Diceware wordlist file
$file = IO.read($config_wordlist_file)

# Get a Diceware passphrase based on config parameters
def get_diceware_passphrase()
	ret = $config_prefix
	for i in 1..$config_words do
		word_index = get_diceware_number()
		match = $file.match /^#{word_index}\s*(.*)$/
		if i.to_i > 1
			ret += $config_separator
		end
		ret += match[1]
	end
	ret += $config_suffix
	return ret
end

# Print the passphrase
puts get_diceware_passphrase()
