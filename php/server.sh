#!/bin/sh

function _get_php_bin {
	if [ -n "$(command -v php)" ]; then
		echo 'php'
	else
		# To find php binary path when it is something like php-8.x
		# This is needed for systems like OpenBSD
		possibilities="/usr/local/bin/php-*
/usr/bin/php-*
$HOME/bin/php-*"
		echo "$possibilities" | while IFS='' read -r possib; do
			output="$(ls $possib &2>/dev/null)"
			if [ "$?" -eq '0' ]; then
				echo "$output" | head -n 1
				break
			fi
		done
	fi
}

php_bin=$(_get_php_bin)

if [ -z "$php_bin" ]; then
	echo 'Error: PHP binary could not be found. PHP might not be installed in the system. If it installed, please report this as a bug with your system info: https://gitlab.com/adnan360/dicemate'
	exit 34
fi

$php_bin -S 127.0.0.1:1167 -t "$(dirname $0)"
