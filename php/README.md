# Dicemate PHP Implementation

Dicemate implementation in PHP scripting language.

Uses cryptographically secure pseudo random number generator function [`random_int()`](https://www.php.net/manual/en/function.random-int.php).

Care has been taken so that PHP functionalities get more focus and to make JavaScript optional. If you want more JS centric features, please check out the `js` version or visit [here](https://adnan360.gitlab.io/dicemate/).

This script generates the passphrases server side. If you are running this on a server controlled by someone else, or if the server machine is physically on someone else's control, they can probably track what you're doing and possibly can snoop in and get your passphrases. Review the traffic flow from your server to the user. You should never use this over plain HTTP unless it is over .onion or other end to end encrypted medium. There is also a possibility of MITM. If you are concerned about these things try running it locally. See below for instructions.

## Usage

There are two ways of running this. You can run this locally with PHP's built in server or if you already have a server running PHP, you can upload this project there and access it.

### Running locally

This is recommended to keep attack surface limited.

To use it simply make sure you have `php` package installed and then run below from a Unix shell:

```sh
./server.sh
```

Or to run manually:

```sh
php -S 127.0.0.1:1167 -t /path/to/this/directory
```

**NOTE:** On some systems, your `php` binary may be something else. e.g. on OpenBSD it might be `php-8.0`, so you might have to use `php-8.0 -S ...` in the command above.

Now access `http://localhost:1167` or `http://127.0.0.1:1167` on your web browser.

### Running on server

Upload the project to your PHP supported server through FTP (SFTP is recommended), SSH, rsync or whatever.

It's optional, but if you want to keep it inside a single directory, copy the `res/eff_large_wordlist.txt` to this directory and change the value of the `$word_list_file` to say just the file name. You can do this on a Unix shell:

```sh
cd /this/directory
cp ../res/eff_large_wordlist.txt ./
sed -i'.bak' -e 's|../res/eff_large_wordlist.txt|eff_large_wordlist.txt|' index.php
```

If you've uploaded this to a directory named `dice` in `example.com`, then access it with `https://example.com/dice/`
