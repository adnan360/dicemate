<?php

/**
 * Rolls dice for some given times.
 *
 * Uses cryptographically secure pseudo random number generator function.
 *
 * @param int $length Length of the random number.
 * @return int
 */
function roll_dice($length = 5) {
	$ret = '';
	for ($i = 0; $i < $length; $i++) {
		$ret .= random_int(1, 6);
	}
	return $ret;
}

/**
 * Get the word from the wordlist given the index number.
 *
 * @param int $number The index number for the word.
 * @return str
 */
function get_word($number) {
	global $word_list;
	if ( empty($word_list) ) {
		return '';
	}
	return @$word_list[$number];
}

/**
 * Get the words data from the wordlist given the index number.
 *
 * @param str $wordlist_filename Wordlist filename.
 * @return array
 */
function load_word_list($wordlist_filename) {
	$ret=array();
	$contents=file_get_contents($wordlist_filename);
	if ( empty($contents) ) {
		return '';
	}
	preg_match_all("/([0-9]+)\s(.*)/", $contents, $matches);
	for ($i = 0; $i < count($matches[1]); $i++) {
		$ret[$matches[1][$i]]=$matches[2][$i];
	}
	return $ret;
}

$word_list_file = '../res/eff_large_wordlist.txt';
$word_list = load_word_list($word_list_file);
$word_count = 7;
if ( intval(@$_GET['words']) > 0 ) {
	$word_count = intval($_GET['words']);
}
$separator = ' ';
if ( @$_GET['separator'] ) {
	$separator = htmlentities($_GET['separator']);
}
$prefix = '';
if ( @$_GET['prefix'] ) {
	$prefix = htmlentities($_GET['prefix']);
}
$suffix = '';
if ( @$_GET['suffix'] ) {
	$suffix = htmlentities($_GET['suffix']);
}

/**
 * Get a passphrase based on parameters (or not).
 *
 * @param int $word_count How many words should the passphrase include.
 * @param str $separator Character(s) to put in between words.
 * @param str $prefix Character(s) to put before the passphrase.
 * @param str $suffix Character(s) to put after the passphrase.
 * @return str
 */
function generate_passphrase($word_count = 7, $separator = ' ', $prefix = '', $suffix = '') {
	$ret = '';
	for ($i = 0; $i < $word_count; $i++) {
		// Get a number
		$number = roll_dice();
		// Get the word
		if ($i > 0) {
			$ret .= $separator;
		}
		$ret .= get_word($number);
	}
	return $prefix.$ret.$suffix;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Dicemate - Diceware passphrase generator (PHP version)</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Diceware passphrase generator written in PHP that can easily be deployed almost anywhere">

	<style type="text/css">
		* {
			margin:0;
			padding:0;
			font-family:sans-serif;
		}
		html, body {
			width: 100%;
			height: 100%;
			display: table;
		}
		.hidden {
			display: none;
		}
		body.has-js .noscript {
			display:none;
		}
		body .passphrase-container {
			display:none;
		}
		body.has-js .passphrase-container {
			display:table-cell;
		}
		.center-container {
			display: table-cell;
			text-align: center;
			vertical-align: middle;
		}
		.passphrase {
			font-size: 40pt;
			padding:5px 20px;
			width:70%;
			height:auto;
			resize: vertical;
			box-sizing: border-box;
		}
		#passphrase-hidden {
			/* visibility:hidden is required instead of display:none to retain height */
			visibility:hidden;
			position:absolute;
			/* To mimic textarea text wrap behavior */
			white-space: pre-wrap;
			word-wrap: break-word;
			/* To account for scrollbar. Calculation: .passphrase.padding-right+10px */
			padding-right: 30px;
		}
		button {
			padding:3px 5px;
		}
		.buttons-container {
			margin:10px 0;
		}
		.row {
			padding:3px 0;
		}
		.row label {
			display:inline-block;
			width:250px;
		}
	</style>
</head>
<body>

	<div class="center-container">
		<p>Your Dicemate Diceware passphrase is:</p>

		<h2><?php echo generate_passphrase($word_count, $separator, $prefix, $suffix); ?></h2>

		<hr/>

		<div class="controls-container">
			<form method="get" id="customize">
				<h3>Customize</h3>
				<div class="formrow">
					<label for="words">Word Count:</label> <input type="text" id="words" name="words" value="<?php echo $word_count; ?>" placeholder="(currently blank)" />
				</div>
				<div class="formrow">
					<label for="separator">Separator:</label> <input type="text" id="separator" name="separator" value="<?php echo $separator; ?>" placeholder="(currently blank)" />
				</div>
				<div class="formrow">
					<label for="prefix">Passphrase Prefix:</label> <input type="text" id="prefix" name="prefix" value="<?php echo $prefix; ?>" placeholder="(currently blank)" />
				</div>
				<div class="formrow">
					<label for="suffix">Passphrase Suffix:</label> <input type="text" id="suffix" name="suffix" value="<?php echo $suffix; ?>" placeholder="(currently blank)" />
				</div>
				<div class="formrow">
					<button type="submit">Submit</button>
				</div>
			</form>
			<div class="buttons-container hidden">
				<button class="button" id="customize-button" href="javascript:void(0);">Customize</button>
				<button class="button" id="generate-button" href="javascript:void(0);">Get another</button>
			</div>
		</div>
		
		<p><a href="https://gitlab.com/adnan360/dicemate">Source code</a></p>
	</div>

	<script>
		// Get element out of an identifier
		function el(query) {
			return document.querySelector(query); 
		}
		// Customize button
		el('#customize-button').addEventListener('click', function(event) {
			el('#customize').classList.toggle("hidden");
		});

		// Customize button
		el('#generate-button').addEventListener('click', function(event) {
			location.reload();
		});

		// If JavaScript enabled, user can use toggle button. So we hide the form.
		el('#customize').classList.add("hidden");
		// If JS is enabled, we show the buttons because they can be usable.
		el('.buttons-container').classList.remove("hidden");
	</script>
</body>
</html>
