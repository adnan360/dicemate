<div align="center">

![Dicemate project icon](https://gitlab.com/uploads/-/system/project/avatar/28328036/dicemate-96.png?width=96 "Dicemate project icon")

# Dicemate

_Diceware passphrase generator written in multiple languages_

</div>


If you know [how Diceware passphrases are generated](https://www.eff.org/dice) you know that it takes some time to roll the dice 4-5 times and look through the word list. Dicemate rolls a virtual dice for you and looks through the wordlist to give you a resulting passphrase. It's instantaneous and effortless.

---
**WARNING:** Although this is convenient, rolling a [real world dice](https://theworld.com/~reinhold/dicewarefaq.html#electronic) is better for increasing randomness. This project was created as a hobby and have not been checked by any security professional. If you are looking for increased protection or your life and safety depends on it, please do not use this project. Although to safeguard against average attacks, this should be fine.
---

The easiest would be to open up the `js/index.html` in a web browser and refresh until you get a good one. It is designed to work entirely offline and within a single file. So you can carry the HTML file around and generate passwords on the go.

Dicemate is also written in many other languages so that it can be used regardless of your language or platform preferences etc. Find the source code in the directory in the parenthesis. Check the `README.md` file inside for instructions on how to use it.

- JavaScript (`js`) - Static HTML+CSS+JS version of Dicemate, single file, easy to carry around and use
- Shell (`sh`) - A Linux/BSD/Unix shell script that can run with /bin/sh, bash, ksh or anything compatible
- PHP (`php`) - A PHP script that can run on a wide range of servers
- Perl (`perl`) - A Perl script that can run offline
- Python (`py`) - A convenient single-file Python 3 script with customizable CLI parameters
- Nim (`nim`) - Fast, in fact probably the fastest compared to other implementations (except Go); needs building, but is very easy to do and outlined in the README.md inside the `nim` directory
- Go (`go`) - Very fast, in fact this is the fastest implementation; needs building, but is very easy to do and outlined in the README.md inside the `go` directory

## Overview

Below is an overview of the implementations of Dicemate in different languages.

| Version          | Needs Build | Web UI¹ | Offline | Uses CSPRNG² | Good for                  |
|------------------|-------------|:-------:|:-------:|:------------:|---------------------------|
|JavaScript / `js` |☒            |☑       |☑        |☑             |Quick use on any browser|
|Shell / `sh`      |☒            |☒       |☑        |☑             |If you have access to Linux/Unix machine|
|Python / `py`     |☒            |☒       |☑        |☑             |If you have Python installed|
|Perl / `perl`     |☒            |☒       |☑        |☑             |If you have Perl installed|
|PHP / `php`       |☒            |☑       |☒³       |☑             |If you have a PHP enabled server|
|Ruby / `ruby`     |☒            |☒       |☑        |☑             |If you have Ruby available|
|Nim / `nim`       |☑            |☒       |☑        |☑             |If you like faster performance and ok with compiling|
|Go / `go`         |☑            |☒       |☑        |☑             |If you like faster performance and ok with compiling|

¹ Use with [precautions](#precautions) and possibly in a private/incognito window on a secure, updated browser in a trusted operating system. \
² [Cryptographically secure pseudorandom number generator](https://en.wikipedia.org/wiki/Cryptographically_secure_pseudorandom_number_generator) is the best mechanism for a computer without [specialized hardware](https://en.wikipedia.org/wiki/Hardware_random_number_generator) to ensure that words in the passphrase are sufficiently random and unpredictable. Properly [reseeded CSPRNGs can arguably beat hardware solutions](https://1password.community/discussion/comment/105778/#Comment_105778). Some even suggest using [hardware to seed CSPRNG](https://cerberus-laboratories.com/blog/random_number_generators/) which should make it even better. \
³ Can be run on a local machine to work offline. Read the README.md inside the directory for details.

## Why?

Diceware passphrases are chosen from a wordlist containing 7k+ words. The wordlist usually contains a list of rememberable words. A single word from this list used as a password can be cracked pretty quickly. But if more and more of these easy to remember words are put together in a password, it becomes difficult for computers to guess it. That's the idea behind Diceware passphrases.

[See this video animation by EFF](https://ssd.eff.org/en/module/animated-overview-how-make-super-secure-password-using-dice)

A known word list should be a problem, right? But it's not, because it is very difficult to guess the combination of words you will use. As [Micah Lee](https://theintercept.com/2015/03/26/passphrases-can-memorize-attackers-cant-guess/) said:

> You’ll need a total of five dice rolls to come up with the first word in your passphrase. What you’re doing here is generating entropy, extracting true randomness from nature and turning it into numbers.
> ...if you choose two words for your passphrase, the size of the list of possible passphrases increases exponentially. There’s still a one in 7,776 chance of guessing your first word correctly, but for each first word there’s also a one in 7,776 chance of guessing the second word correctly, and **the attacker won’t know if the first word is correct without guessing the entire passphrase.**

This is why this method rules. And the best part is it keeps the passphrase memorable even though it is difficult for computers to guess:

> Not too bad for a passphrase like “bolt vat frisky fob land hazy rigid,” which is entirely possible for most people to memorize. Compare that to “d07;oj7MgLz’%v,” a random password that contains slightly less entropy than the seven-word Diceware passphrase but is significantly more difficult to memorize.

Traditionally we know passwords as just pass-words. But the word pass-**word** gives an idea that you need to enter a single word as a password - which is really insecure and should not be used. So the output that Diceware produces is called a "passphrase". The "phrase" part indicates that it contains more than one word. Many security conscious software has started to call their password fields "passphrase" years ago due to how easy it is for users to ruin their security based on the assumption that they're asked to enter a single word. Passphrases are better and enter passphrase regardless of what the field name appears on screen.

So the result of the Diceware process is a very strong passphrase despite it being very memorable. More importantly these passphrases are so strong that average adversaries can't crack them easily. Just try to use 7 or more words if possible. 6-word passphrases are fine for most users, but more is recommended. Increasing the word count from 6 to 7 increases the complexity of the passphrase significantly. That's why Dicemate has a default of 7 words to begin with. To increase the difficulty, add a separator in between words and if possible add prefix and suffix. This is still easy to remember but adds more complexity.

The wordlist used throughout the project almost exclusively is made by EFF. But there are other wordlists out there. Some are available in [other languages](https://theworld.com/~reinhold/diceware.html#Diceware%20in%20Other%20Languages|outline) as well.

## Precautions

**WARNING:** These are not written by a security professional. Please read the [warning portion here](../docs/precautions.md) first, thanks.

While Diceware passphrases are reasonably secure, you should apply some caution when using. Here are some of the basic ones:

- Do not let others see your passphrase; be careful
- Do not re-use the same passphrase on multiple website/software, generate a new one for each and use a secure password manager to save them
- Do not change words yourself, humans are not good at randomizing stuff
- Write down your passphrase in pen and paper and store it somewhere safe, because there's not always a forgot password option and similar words can get mixed up in memory
- Make sure you have at least 7 words to be adequately hard to crack
- Never store the passphrase unencrypted (esp. on SSDs and flash drives); use at least something like [EncFS](https://en.wikipedia.org/wiki/EncFS)
- Do not use a computer used by others or a public computer for Dicemate
- Use an updated secure operating system
- In case of Web UIs:
	- Use Private/Incognito window (only protects history) or TOR Browser (protects history+identity)
	- Use HTTPS or end-to-end-encrypted (such as .onion) URLs for non local instances
	- Strictly avoid using Dicemate hosted on untrusted servers; try to use locally instead
	- Use updated secure web browser without questionable addons/extensions/changes
- Move your mouse in between generating passphrases to improve randomness
- Do not discuss the methods/parameters you use to generate the passphrases with anyone

Above is just a summary and does not have every important detail. It is recommended that you check [the detailed precautions document](../docs/precautions.md) for details and more helpful guidelines.

## Resources

- [https://theworld.com/~reinhold/diceware.html](https://theworld.com/~reinhold/diceware.html)
- [Passphrases That You Can Memorize — But That Even the NSA Can’t Guess](https://theintercept.com/2015/03/26/passphrases-can-memorize-attackers-cant-guess/)
- [Generating Passphrases with Dice One-Pager Handout](https://www.eff.org/document/diceware-one-pager-handout)
- [How to Roll a Strong Password with 20-Sided Dice and Fandom-Inspired Wordlists](https://www.eff.org/deeplinks/2018/08/dragon-con-diceware)

## License

Unless otherwise mentioned, these are applicable:

- Code exclusive to this repo: [CC0 1.0 Universal](https://creativecommons.org/publicdomain/zero/1.0/)
- Long Wordlist from EFF: [Creative Commons Attribution License](https://creativecommons.org/licenses/by/3.0/us/) ([Details](https://www.eff.org/copyright))
