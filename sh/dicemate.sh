#!/bin/sh

# dice.sh
# A shell script to generate diceware passphrases.
#
# Parameters:
#   $1: Number of words wanted in the passphrase. Default: 7 (optional)
#   $2: Separator text to be in middle of words. Default: <space> (optional)
#
# Usage:
#   sh dicemate.sh
#   bash dicemate.sh 7
#   ksh dicemate.sh 8 '-'
#
# License:
#   This script: CC0 (https://creativecommons.org/publicdomain/zero/1.0/);
#   Wordlist is by EFF: CC-BY (https://creativecommons.org/licenses/by/3.0/us/)

cd $(dirname $0)

wordlist_cache_file="$HOME/.cache/$(basename $0)/wordlist.txt"
wordlist_file="../res/eff_large_wordlist.txt"
wordlist_url='https://www.eff.org/files/2016/07/18/eff_large_wordlist.txt'

# Get random output of dice rolls up to a certain length
# $1: length
_get_dicerolls() {
	local i=1; while [ $i -le $1 ]; do
		# Get random value
		# Ref: https://stackoverflow.com/a/4676597
		local value=$(od -An -N2 -tu2 /dev/urandom)
		ret="$ret""$(expr $value % 5 + 1)"
		i=$(($i + 1))
	done
	echo "$ret"
}

# Input
# Word count. Default: 7
if [ -z "$1" ]; then
	iterations=7
else
	iterations=$1
fi
# Separator. Default: <space>
if [ -z "$2" ]; then
	separator=' '
else
	separator="$2"
fi

# Download word file if it doesn't exist
if [ ! -f "$wordlist_file" ]; then
	echo 'Diceware wordlist not found. Downloading...'
	mkdir -p "$(dirname $wordlist_cache_file)"
	curl -L "$wordlist_url" -o "$wordlist_cache_file" || wget "$wordlist_url" -o "$wordlist_cache_file"
	if [ "$?" -eq '0' ]; then
		echo "Wordlist saved to: ${wordlist_cache_file}"
		wordlist_file="$wordlist_cache_file"
	else
		echo "Error: Wordlist could not be downloaded!"
		exit 56
	fi
fi

# Getting output
dice_numbers=''
i=1; while [ $i -le $iterations ]; do
	dice_numbers="$dice_numbers"$(_get_dicerolls 5)"\n"
	i=$(($i + 1))
done
echo '== The diceware random numbers are:'
echo "$dice_numbers"

echo '== The resulting passphrase is:'
echo "$dice_numbers" | while IFS="" read -r num; do
	if [ -n "$num" ]; then
		# Skip separator before the first word
		[ -n "$passphrase" ] && echo -n "$separator"
		passphrase=$(grep "^${num}" "$wordlist_file" | awk '{ print $2 }')
		echo -n "$passphrase"
	fi
done
# Print a carriage return at the end to cancel out effect of earlier echo -n calls.
echo ''
