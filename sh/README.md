# Dicemate Shell

A shell script implementation that can be run with `/bin/sh`, bash, ksh or any other Unix compatible shell.

## Requirements

- Supports any operating system with a unix compatible shell support. Non Unix platforms may be supported through third party solutions, such as cygwin (but not tested). Using this on a proprietary system such as, Windows, Mac OS X or any untrusted privacy disrespecting system is not recommended.
- `curl` or `wget`
- working `od` and `expr` binary

# Usage

To run it:

```sh
sh /path/to/dicemate.sh
# or
cd /path/to/the/script
sh dicemate.sh
# or if executable permission is there
cd /path/to/the/script
./dicemate.sh
```

To customize:

```sh
sh dicemate.sh 8
sh dicemate.sh 9 '-'
```

The first parameter being the word count, second being the word separator.

If you want to install it on a Linux/Unix system, you can run something like:

```sh
$ su
# cp dicemate.sh /usr/local/bin/dicemate
# exit
```

Run `echo $PATH` and make sure that `/usr/local/bin` is listed there. Now you should be able to run `dicemate` from any directory on the system.

Make sure to maintain [general precautions](../README.md#precautions) when using.
