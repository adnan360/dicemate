# Dicemate Perl

## Preparation

Run `perl -v` and make sure that it returns a version number. If it does, it means it is installed. Perl is usually installed in [most](https://unix.stackexchange.com/questions/568058/is-there-a-unix-like-system-that-doesnt-come-with-perl) Linux/Unix systems. Otherwise [install perl](https://www.perl.org/get.html).

Perl implementation of Dicemate uses [Math::Random::Secure](https://metacpan.org/pod/Math::Random::Secure) for cryptographically secure random numbers for passphrases to be sufficiently unpredictable. To install it:

```sh
cpan -T Math::Random::Secure
```

`-T` is to skip tests and just install.

If this is your first time running the command it may ask you some questions like where to save packages and should it add to ~/.bashrc some commands for cpan to work comfortably. You can just press enter when questions are asked. This will install and prepare the environment.

If you are an expert at this and have used cpan before, please install the package like you normally do.

If you don't have `cpan` installed, try searching for `cpanm` or `cpanminus` on your operating system's package repo. If you don't have it there you can just run [this command](https://foswiki.org/Support/HowToInstallCpanModules#Install_CPAN_modules_into_your_local_Perl_library_using_61App::cpanminus_61):

```sh
curl -L https://cpanmin.us | perl - App::cpanminus
```

Then install the module with:

```sh
cpanm -n Math::Random::Secure
```

In the case of cpanm, `-n` is to skip tests.

## Usage

`cd` into or open a terminal in this directory. Then:

```sh
./dicemate.pl
# or
perl dicemate.pl
```

**NOTE:** The first command needs executable permission. If it complains permission denied or something, run `chmod +x dicemate.pl`.

Optionally, you can pass command line parameters to customize the passphrase.

Examples:

```sh
$ ./dicemate.pl
someword someword someword someword someword someword someword
$ ./dicemate.pl -w 9 -s '/'
someword/someword/someword/someword/someword/someword/someword/someword/someword
$ ./dicemate.pl -a '_*' -z '*_' -s '-'
_*someword-someword-someword-someword-someword-someword-someword*_
```

Run `./dicemate.pl -h` for more details and options.
