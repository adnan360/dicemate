#!/usr/bin/env perl
use warnings;
use strict;

# To handle CLI parameters
use Getopt::Long;

# Default config
# Word count
my $config_words = 7;
# Separator between words
my $config_separator = ' ';
# Prefix that sits before passphrase
my $config_prefix = '';
# Suffix that sits after passphrase
my $config_suffix = '';
# Length of index number on wordlist file
my $config_number_length = 5;
# Wordlist file
my $config_wordlist_file = '../res/eff_large_wordlist.txt';

# Shows up when --help or -h is passed
sub help_text {
	print("usage: dicemate.pl [-h] [-w WORD_COUNT] [-s SEPARATOR] [-a PREFIX] [-z SUFFIX] [-f WORDLIST_FILE]
                   [-l LENGTH]

Generates a Diceware passphrase based on parameters.

optional arguments:
  -h, --help            show this help message and exit
  -w WORD_COUNT, --word-count WORD_COUNT
                        word count
  -s SEPARATOR, --separator SEPARATOR
                        separator between words
  -a PREFIX, --prefix PREFIX
                        prefix before passphrase
  -z SUFFIX, --suffix SUFFIX
                        suffix before passphrase
  -f WORDLIST_FILE, --wordlist-file WORDLIST_FILE
                        Diceware wordlist filename
  -l LENGTH, --index-length LENGTH
                        Diceware wordlist index length\n");
	exit;
}

# Process CLI parameters and update config values as necessary
GetOptions ("w|word-count=i"    => \$config_words,
			"s|separator=s"     => \$config_separator,
			"a|prefix=s"        => \$config_prefix,
			"z|suffix=s"        => \$config_suffix,
			"f|wordlist-file=s" => \$config_wordlist_file,
			"l|index-length=i"  => \$config_number_length,
			"h|help"            => \&help_text)
or die("Error in command line arguments. Please review and try again.\n");

# Get a dice roll number (1 through 6).
sub get_dice_roll() {
	use Math::Random::Secure qw(irand);
	return irand(6) + 1;
}

# Get a Diceware index number which can be looked up on wordlist file.
sub get_diceware_number() {
	my $ret = '';
	for ( 0 .. $config_number_length - 1 ) {
		$ret = $ret . get_dice_roll();
	}
	return $ret;
}

# Open file for running the search.
# "<" indicates read only.
open( my $file, "<", $config_wordlist_file ) or die("File $config_wordlist_file not found");

# Get a Diceware passphrase based on config parameters
sub get_diceware_passphrase() {
	my $ret = $config_prefix;
	for my $i ( 0 .. $config_words - 1 ) {
		# Get the index number for word
		my $index = get_diceware_number();
		# Rewind to search from first line on the same file header
		# without reopening every time.
		seek( $file, 0, 0 ); 
		while( my $search_string = <$file> ) {
			# Run search with regex
			if( $search_string =~ /$index\s*(.*)/ ) {
				# Add separator to return string
				if( $i > 0) {
					$ret = $ret . $config_separator;
				}
				# Add word to return string
				$ret = $ret . $1;
			}
		}
	}
	$ret = $ret . $config_suffix;
}

# Print the passphrase
print get_diceware_passphrase() . "\n";

# Close the file because it is not needed anymore
close( $file );
